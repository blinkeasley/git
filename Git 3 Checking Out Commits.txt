+ checking out "commits" /undoing things/going back in time
+ master branch is the master head

+ to view a list of log commits
git log --oneline

+to checkout commit type the unique id base on the git log
git checkout {unique id}

+to go back to the master branch checkout the master branch
git checkout master